# Установка базового образа Ubuntu
FROM ubuntu:22.04
WORKDIR /home/vagrant/es
RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y tzdata && \
    ln -fs /usr/share/zoneinfo/Asia/Almaty /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get clean
# Обновление и установка необходимых пакетов
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    software-properties-common \
    curl \
    gnupg2 \
    build-essential \
    && \
    rm -rf /var/lib/apt/lists/*

# Установка PHP 8.1 и необходимых расширений
RUN add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    php8.1 \
    php8.1-fpm \
    php8.1-cli \
    php8.1-mysql \
    php8.1-sqlite3 \
    php8.1-odbc \
    php8.1-xml \
    php8.1-curl \
    php8.1-mbstring \
    php8.1-zip \
    php8.1-bcmath \
    php8.1-gd \
    php8.1-intl \
    php-pear \
    php8.1-dev \
    && \
    rm -rf /var/lib/apt/lists/* && \
    update-alternatives --set php /usr/bin/php8.1

# Установка модуля pdo_sqlsrv
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    unixodbc-dev \
    g++ \
    && \
    pear config-set php_ini /etc/php/8.1/cli/php.ini && \
    pecl install pdo_sqlsrv-5.12.0 && \
    echo "extension=pdo_sqlsrv.so" > /etc/php/8.1/cli/conf.d/20-pdo_sqlsrv.ini && \
    echo "extension=pdo_sqlsrv.so" > /etc/php/8.1/fpm/conf.d/20-pdo_sqlsrv.ini && \
    rm -rf /var/lib/apt/lists/*

# Установка модуля pdo_mysql
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    default-mysql-client && \
    apt-get install -y --no-install-recommends \
    php8.1-mysql && \
    echo "extension=pdo_mysql.so" > /etc/php/8.1/cli/conf.d/20-pdo_mysql.ini && \
    echo "extension=pdo_mysql.so" > /etc/php/8.1/fpm/conf.d/20-pdo_mysql.ini && \
    rm -rf /var/lib/apt/lists/*
# Установка модуля pdo_odbc
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    unixodbc-dev && \
    apt-get install -y --no-install-recommends \
    g++ && \
    apt-get install -y --no-install-recommends \
    php8.1-odbc && \
    echo "extension=pdo_odbc.so" > /etc/php/8.1/cli/conf.d/20-pdo_odbc.ini && \
    echo "extension=pdo_odbc.so" > /etc/php/8.1/fpm/conf.d/20-pdo_odbc.ini && \
    rm -rf /var/lib/apt/lists/*

# Установка Nginx
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    nginx && \
    rm -rf /var/lib/apt/lists/*

# Установка Node.js и npm
RUN su -c 'curl -sL https://deb.nodesource.com/setup_18.x | bash -' && \
    apt-get install  -y --no-install-recommends \
    nodejs
#    rm -rf /var/lib/apt/lists/*

# Очистка кэша и временных файлов
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV DEBIAN_FRONTEND=noninteractive

#RUN echo "msodbcsql18 msodbcsql/accept_eula select true" | debconf-set-selections && \
#    curl https://packages.microsoft.com/keys/microsoft.asc | tee /etc/apt/trusted.gpg.d/microsoft.asc && \
#    curl https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/prod.list | tee /etc/apt/sources.list.d/mssql-release.list && \
#    apt-get update && \
#    apt-get install -y --no-install-recommends \
#    msodbcsql18 && \
#    apt-get install -y --no-install-recommends \
#    mssql-tools18

#RUN echo 'export PATH="$PATH:/opt/mssql-tools18/bin"' >> ~/.bashrc && source ~/.bashrc && \
#    apt-get install -y unixodbc-dev
# Копирование конфигурационных файлов
COPY nginx/default /etc/nginx/sites-available/default
COPY ssl /etc/ssl
COPY es .
RUN chmod -R 777 /home/vagrant/es && chown -R www-data:www-data /home/vagrant/es
# Определение рабочей директории
#WORKDIR /var/www/html
#WORKDIR /home/vagrant/es
# Открытие портов
EXPOSE 8080
EXPOSE 8443

# Запуск служб
CMD service php8.1-fpm start && nginx -g "daemon off;"
