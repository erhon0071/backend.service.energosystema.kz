<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\OrderController;
use App\Http\Controllers\Api\V1\BillingController;
use App\Http\Controllers\Api\V1\BillingEmployeeController;
use App\Http\Controllers\Api\V1\ElectricMetersController;
use App\Http\Controllers\Api\V1\ServicesController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'v1'], function(){

   Route::apiResource('order', OrderController::class);

   Route::apiResource('billing', BillingController::class);

   Route::apiResource('billing-employee', BillingEmployeeController::class);

   Route::apiResource('electric-meters', ElectricMetersController::class);

   Route::apiResource('services', ServicesController::class);

});

