<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pd_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_order');
            $table->integer('id_services');
            $table->integer('id_status');
            $table->integer('payment_type');
            $table->string('service_reason', 255);
            $table->date('task_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
