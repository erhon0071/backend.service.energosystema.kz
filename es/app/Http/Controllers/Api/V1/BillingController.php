<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\BillingCollection;
use App\Http\Resources\V1\BillingResource;
use App\Http\Resources\V1\OrderCollection;
use Illuminate\Http\Request;
use App\Models\Billing;

class BillingController extends Controller
{
    public function index()
    {
        return new BillingCollection(Billing::where('DealType_Code', 43)->take(5)->get());
    }

    public function show($dealnum)
    {

        $billing = Billing::join('vwElectricMeters', 'vwElectricMeters.Serial_Number', '=', 'esPoints.Serial_Number')
            ->where('Deal_Num', $dealnum)
            ->where('DealType_Code', 43)
            ->get();

        foreach ($billing as $data) {
            $serialNumbers[] = $data->Serial_Number;
            $phaseCount[] = $data->Phase_Count;
        }

        return  [

            "data" => [
                "Deal_num" => $billing->first()->Deal_Num,
                "Subject_Name" => $billing->first()->Subject_Name,
                "Address_Name" => $billing->first()->Address_Name,
                "Serial_Numbers" => $serialNumbers,
                "Phase_Count" => $phaseCount,

            ],
        ];

        /*if ($billing->count() === 1) {
           return new BillingResource($billing->first());
       }*/

        /* return BillingResource::collection($billing);*/
    }


}
