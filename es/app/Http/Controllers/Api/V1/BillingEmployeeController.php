<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\EmployeeCollection;
use App\Models\BillingEmployee;
use Illuminate\Http\Request;

class BillingEmployeeController extends Controller
{

    public function index(){
        return new EmployeeCollection(BillingEmployee::where('Department_ID', 8406432)
            ->where('Position_ID', 192)
            ->whereNull('Date_To ')
            ->get());

    }
}
