<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\ElectricMetersResource;
use App\Models\ElectricMeters;
use Illuminate\Http\Request;

class ElectricMetersController extends Controller
{
    public function show($meters)
    {
        $electric = ElectricMeters::
        where('Serial_Number', $meters)->get();

        return new ElectricMetersResource($electric->first());
    }
}
