<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreServicesRequest;
use App\Http\Resources\V1\ServicesCollection;
use App\Http\Resources\V1\ServicesResource;
use App\Models\test;
use Illuminate\Http\Request;
use App\Models\Services;

class ServicesController extends Controller
{
    public function store(StoreServicesRequest $request)
    {
        //$services = Services::create($request->validated());

        $services = $request->validated();
        foreach ($services as $projectData) {
            Services::create($projectData);
        }

    }





    public function show($phase)
    {
        $services = test::where('phase_count', $phase)->get();
        return ServicesResource::collection($services);
    }

}
