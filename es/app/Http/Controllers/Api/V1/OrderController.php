<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\V1\OrderCollection;
use App\Http\Resources\V1\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        return new OrderCollection(Order::paginate(5));
    }

    public function show(Order $order)
    {
       return new OrderResource($order);
    }

    public function store(StoreOrderRequest $request)
    {
        $user = Order::create($request->validated());
        return response()->json($user, 201);
    }

    public function update(StoreOrderRequest $request, Order $order){
        $order->update($request->validated());
        return response()->json($order, 201);
    }

    public function destroy(Order $order)
    {
        $order->delete();
        return response()->json($order, 201);
    }


}
