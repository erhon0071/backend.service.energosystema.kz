<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BillingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
       return [
            "Deal_num" => $this->Deal_Num,
            "Subject_Name" => $this->Subject_Name,
            "Address_Name" => $this->Address_Name,
            "Serial_Number" => $this->Serial_Number,
        ];

       /* return [
             [
                "Deal_num" => $this->Deal_Num,
                [
                    "Subject_Name" => $this->Subject_Name,
                    "Address_Name" => $this->Address_Name,
                    "Serial_Numbers" => $this->Serial_Number,
                ],
            ],
        ];*/
    }

}
