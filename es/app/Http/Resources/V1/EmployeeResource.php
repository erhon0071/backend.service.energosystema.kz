<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        //return parent::toArray($request);

        return [
            'Employee_ID' => $this->Employee_ID,
            'Full_Name' => $this->Full_Name,
            'Position_Name' => $this->Position_Name,
            'Short_Name' => $this->Short_Name,
        ];
    }
}
