<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules()
    {
        return [
            'account_number' => 'required|string|max:50',
            'full_name' => 'required|string|max:255',
            'address' => 'required|string|max:500',
            'meter_number' => 'required|string|max:50',
            'rn_date' => 'nullable|date',
            'uuid' => 'nullable',
        ];
    }
}
