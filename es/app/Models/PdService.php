<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PdService extends Model
{
    use HasFactory;
    protected $table = 'pd_service';
    protected $fillable = [
        'id',
        'id_order',
        'id_services',
        'id_status',
        'payment_type',
        'service_reason',
        'task_date',
    ];


}
