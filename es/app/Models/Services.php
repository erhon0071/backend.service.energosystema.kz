<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    use HasFactory;

    protected $table = 'servicess';
    protected $fillable = [
        'id',
        'uuid',
        'tittle',
        'phase_count',
        'amount',
        'amount_words',
        'end_date'
    ];
}
